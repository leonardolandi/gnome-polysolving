# Maintainer: <lellolandi@gmail.com>
pkgname=polysolving
pkgver=3.28.2
pkgrel=1
pkgdesc="Localize, approximate and calculate multiplicity of polynomial and polynomial system solutions"
arch=('i686' 'x86_64')
url="https://gitlab.com/leonardolandi/gnome-polysolving"
license=('GPL3')
depends=('gettext' 'gtk3>=3.20.0' 'hicolor-icon-theme' 'lapack>=3.7.0' 'python>=3.0.0' 'python-gobject>=3.0.0')
optdepends=('Macaulay2: You can also use the "Generic" version, downloadable from the official website')
makedepends=('git' 'unzip')
source=("git+https://gitlab.com/leonardolandi/gnome-polysolving")
md5sums=('SKIP')

package() {

	cd "$srcdir"/gnome-polysolving
	mv ./* $pkgdir
	cd $pkgdir

	mkdir -p usr/lib/PolySolving/python
	mv lib/* usr/lib/PolySolving/python/
	sed -i "s|LANGUAGE_FOLDER|\"/usr/share/locale\"|g" usr/lib/PolySolving/python/translation.py
	
	mkdir -p usr/lib/PolySolving/M2
	mv M2/* usr/lib/PolySolving/M2/

	mkdir -p usr/lib/PolySolving/glade
	mv ui/* usr/lib/PolySolving/glade/

	for i in 16x16 32x32 48x48 64x64 128x128 256x256 scalable symbolic; do
		mkdir -p usr/share/icons/hicolor/$i/apps
		mv icons/$i/* usr/share/icons/hicolor/$i/apps/
	done
	
	for i in po/*.po; do
		h=${i%.po}
		j=${h##*/}
		mkdir -p usr/share/locale/$j/LC_MESSAGES
		msgfmt $i -o usr/share/locale/$j/LC_MESSAGES/polysolving.mo
	done

	mkdir -p usr/bin
	mv bin/polysolving usr/bin/
	sed -i "s|appdir|/usr/lib/PolySolving|g" usr/bin/polysolving
	chmod +x usr/bin/polysolving

	mkdir -p usr/share/applications
	mv PolySolving.desktop usr/share/applications/

	mkdir -p usr/share/licenses/PolySolving
	mv LICENSE usr/share/licenses/PolySolving/

	for i in /home/*/; do
		user=$(basename $i)
		mkdir -p home/$user/.PolySolving
		chmod 700 home/$user
		chmod 777 -R home/$user/.PolySolving
	done
	
	rm -R bin
	rm -R lib
	rm -R M2
	rm -R ui
	rm -R icons
	rm -R po
	rm Makefile
	rm README.md
	
}
