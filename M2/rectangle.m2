str := get inputfile;
arr := value(str);
print concatenate("\n--> Received search coordinates of rectangle with sides (",toString(arr#0),",",toString(arr#1),") and (",toString(arr#2),",",toString(arr#3),")");
bord := border(arr#0,arr#1,arr#2,arr#3);
outputfile << "";
outputfile << count << endl;
if bord == {} then (
	outputfile << 6 << endl;
	outputfile << toString(arr) << endl;
	sol := rectangle(arr#0,arr#1,arr#2,arr#3);
	outputfile << sol << endl;
	print "--> Calculated number of solutions inside rectangle";
	vals := new List;
	appx := new List;
	if sol > 0 and arr#4 != 0 then (
		vals = subsquare(arr#0,arr#1,arr#2,arr#3,vals);
		print "--> Isolated solutions inside sub-rectangles";
		for rect in vals do (
			appx = append(appx,quadrisect(rect#0,rect#1,rect#2,rect#3,arr#4));
			print "--> Calculated approximation of a solution";
		);
		print concatenate("--> Calculated approximations with precision ",toString(arr#4));
		outputfile << toString(vals) << endl;
		outputfile << toString(appx) << endl;
	);
	if sol > 0 and arr#5 != 0 then (
		if #vals == 0 then (
			vals = subsquare(arr#0,arr#1,arr#2,arr#3,vals);
			outputfile << toString(vals) << endl;
		);
		mults := new List;
		for i from 0 to #vals-1 do (
			p := arr#4;
			square := vals#i;
			if #appx > 0 then (
				app := appx#i;
				square = {app#0-p,app#0+p,app#1-p,app#1+p};
			);
			m := multip2(square);
			while m == 0 do (
				p = p/10;
				app = quadrisect(vals#i#0,vals#i#1,vals#i#2,vals#i#3,p);
				m = multip2(app,p);
			);
			mults = append(mults,m);
		);
		print "--> Calculated multiplicities";
		outputfile << toString(mults) << endl;
	);
) else (
	outputfile << 7 << endl;
	outputfile << toString(arr) << endl;
	outputfile << bord#0 << endl;
	outputfile << bord#1 << endl;
	print "--> Solutions on rectangle border lines";
);
outputfile << close;
count = count +1;
