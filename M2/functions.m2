count = 0;
print "\n--> This is a Macaulay2 interactive console.\n--> Global variables R,B,D,I,Ix,Iy are reserved for Ring, Basis of Quotient Ring, Dimension of Quotient Ring, Ideal and Elimination Ideals respectively. Please do not overwrite.";

-- This function must be called after a new ideal I is given. It stores new global variables B and D, resp. the basis of the quotient and its dimension and elimination ideals in the case of two variables.

initialize = () -> (
	
	B = sub(basis (R/I),R);
	D = numcols B;
	if numgens R == 2 then (
		Ix = eliminate(y,I);
		Iy = eliminate(x,I);
	);

);

-- Given a polynomial h, it returns its companion matrix.

companion = h -> (

	if numgens R == 1 then (
		comp1 := sub(contract(transpose B,B_(0,0)*h%I),{x=>0_QQ});
		for i from 1 to (D-1) do comp1 = comp1|sub(contract(transpose B,(B_(0,i)*h%I)),{x=>0_QQ});
		return comp1;
	) else (
		comp2 := sub(contract(transpose B,B_(0,0)*h%I),{x=>0_QQ,y=>0_QQ});
		for i from 1 to (D-1) do comp2 = comp2|sub(contract(transpose B,(B_(0,i)*h%I)),{x=>0_QQ,y=>0_QQ});
		return comp2;
	);

);

-- Given a polynomial h, it returns the trace form.

bezoutian = h -> (
	
	comph := companion(h);
	b := mutableMatrix(R,D,D);
	for i from 0 to D-1 do (
		for j from 0 to i do (
			b_(i,j) = trace(companion(h*B_(0,i)*B_(0,j)));
			b_(j,i) = b_(i,j);
		);
	);
	bez := matrix b;
	return bez;
	
);

-- The input must be a matrix M (the trace form matrix in this context). It returns a 3-dimensional sequence containing positive, negative and zero eigenvalues of M, using the Déscartes's rule of signs.

signature = M -> (

	d := det(M-x*id_(R^D));
	zer := 0;
	g := d;
	while gcd(g,x) != 1 do (
		zer = zer + 1;
		g = diff(x,g);
	);
	p := coefficient(x^zer,d);
	pos := 0;
	for i from 1 to D do (
		if coefficient(x^i,d)*p < 0 then (
			pos = pos + 1;
			p = coefficient(x^i,d);
		);
	);
	neg := D - zer - pos;
	return (pos,neg,zer);
	
);

-- This function applies the Sylvester-Hermite's theorem. h is the polynomial in the theorem and n is an integer, from 1 to 3 that indicates which point of the theorem must be returned (point number 2 is the most used).

sylvester = (h,n) -> (

	if h%I == 0 then return 0;
	bez := bezoutian(h);
	s := signature(bez);
	s1 := s;
	if h != 1 then (
		bez1 := bezoutian(1);
		s1 = signature(bez1);
	);
	if n == 1 then return rank(bez);
	if n == 2 then return s#0-s1#1;
	if n == 3 then return s#1-s1#1;
	
);

-- Returns the radical of I, using the reduction method on the generators of each elimination ideal.

rad = () -> (

	Ix := eliminate(y,I); fx := Ix_0;
	while gcd(fx,diff(x,fx)) != 1 do fx = numerator(fx/gcd(fx,diff(x,fx)));
	Iy := eliminate(x,I); fy := Iy_0;
	while gcd(fy,diff(y,fy)) != 1 do fy = numerator(fy/gcd(fy,diff(y,fy)));
	sol := I + ideal(fx,fy);
	return sol;
 	
);

-- This function is needed when the localization of solution in a rectangle is requested. It states if one the border lines of the given intervals contains a solution of the system.

border = (ax,bx,ay,by) -> (

	valax := sub(Ix_0,x=>ax);
	valbx := sub(Ix_0,x=>bx);
	valay := sub(Iy_0,y=>ay);
	valby := sub(Iy_0,y=>by);
	if valax == 0 then return {ax,x};
	if valbx == 0 then return {bx,x};
	if valay == 0 then return {ay,y};
	if valby == 0 then return {by,y};
	return {};
	
);

-- This returns the number of real solutions inside a rectangle delimited by ax,bx,ay,by with obvious notation.

rectangle = (ax,bx,ay,by) -> (

	nlist := {
		sylvester(-(x-ax)*(x-bx),2),
		sylvester(-(y-ay)*(y-by),2),
		sylvester((x-ax)*(x-bx)*(y-ay)*(y-by),2),
		sylvester(-(x-ax)*(x-bx)*(y-ay)*(y-by),2)
	};
	S := QQ[a,b,c,d]; use S;
	J := ideal(a+b-nlist#0,a+c-nlist#1,a+d-nlist#2,b+c-nlist#3);
	Jel := eliminate({b,c,d},J);
	use R;
	sol := -sub(Jel_0,a=>0);
	return sol;
	
);

-- This function returns a list of inner intervals of (a,b) containing exactly one solution. The input vals is a list of intervals, empty in most cases.

isolate = (a,b,vals) -> (
	
	s := sylvester(-(x-a)*(x-b),2);
	if s == 0 then return vals;
	if s == 1 then (
		print "--> A solution has been isolated";
		return append(vals,{a,b});
	);
	len := (b-a)/2;
	while sub(I_0,x=>a+len) == 0 do len = len/2;
	vals = isolate(a,a+len,vals);
	vals = isolate(a+len,b,vals);
	return sort vals;
	
);

-- Tha same function of "isolate" in two variables.

subsquare = (ax,bx,ay,by,vals) -> (

	r := rectangle(ax,bx,ay,by);
	if r == 0 then return vals;
	if r == 1 then (
		print "--> A solution has been isolated";
		return append(vals,{ax,bx,ay,by});
	);
	lenx := (bx-ax)/2; leny := (by-ay)/2;
	while sub(Ix_0,x=>ax+lenx) == 0 do lenx = lenx/2;
	while sub(Iy_0,y=>ay+leny) == 0 do leny = leny/2;
	vals = subsquare(ax,ax+lenx,ay,ay+leny,vals);
	vals = subsquare(ax+lenx,bx,ay,ay+leny,vals);
	vals = subsquare(ax,ax+lenx,ay+leny,by,vals);
	vals = subsquare(ax+lenx,bx,ay+leny,by,vals);
	return sort vals;
	
);

-- Approximation of the unique solution in a given interval (a,b) with the requested precision (example: prec=1/100). The "isolate" function must be called previously in order to prevent the existance of two or more solutions inside the given interval.

bisect = (a,b,prec) -> (
	
	mid := (a+b)/2;
	if (b-a) < 2*prec then (
		print "--> Calculated approximation of a solution";
		return mid;
	);
	if sub(I_0,x=>mid) == 0 then (
		print "--> Calculated approximation of a solution";
		return mid;
	);
	if sylvester(-(x-a)*(x-mid),2) == 0 then return bisect(mid,b,prec)
	else return bisect(a,mid,prec);
	
);

-- Approximation in a given rectangle. The same as "bisect" in two variables.

quadrisect = (a,b,c,d,prec) -> (

	lenx := (b-a)/2; leny := (d-c)/2;
	midx := a+lenx; midy := c+leny;
	while sub(Ix,x=>midx) == 0 do (
		lenx = lenx/2;
		midx = a+lenx;
	);
	if (b-a) >= 2*prec then (
		if rectangle(a,midx,c,d) == 0 then return quadrisect(midx,b,c,d,prec)
		else return quadrisect(a,midx,c,d,prec);
	);
	while sub(Iy,y=>midy) == 0 do (
		leny = leny/2;
		midy = c+leny;
	);
	if (d-c) >= 2*prec then ( 
		if rectangle(a,b,c,midy) == 0 then return quadrisect(a,b,midy,d,prec)
		else return quadrisect(a,b,c,midy,prec);
	);
	return {midx,midy};
	
);

-- An interval (obtained from the "isolate" function) is given; the relative multiplicity is returned.
	
multip = (a,b) -> (
	
	oldI := I; oldB := B; oldD := D;
	m := 1;
	f := I_0;
	while gcd(f,diff(x,f)) != 1 do (
		f = gcd(f,diff(x,f));
		I = ideal(f); initialize();
		if sylvester(-(x-a)*(x-b),2) > 0 then m = m+1
		else break;
	);
	I = oldI; B = oldB; D = oldD;
	print "--> Calculated multiplicity of a solution";
	return m;
);

-- Multiplicity in two variables. Input is a single element of the list obtained with "subsquare". Multiplicity is returned or 0 if we can't isolate the only solution with the given precision.

multip2 = (square) -> (
	
	oldI := I;
	for i from 0 to 4 do (
		a := random(QQ);
		h := a*x-y;
		v1 := sub(h,{x=>square#0,y=>square#3});
		v2 := sub(h,{x=>square#1,y=>square#2});
		M := companion(h);
		if sylvester(-(h-v1)*(h-v2),2) == 1 then (
			R = QQ[x];
			d := det(M-x*id_(R^D));
			I = ideal(d); B = sub(basis (R/I),R); D = numcols B;
			m := 1;
			while gcd(d,diff(x,d)) != 1 do (
				d = gcd(d,diff(x,d));
				I = ideal(d); B = sub(basis (R/I),R); D = numcols B;
				if sylvester(-(x-v1)*(x-v2),2) > 0 then m = m+1
				else break;
			);
			R = QQ[x,y]; I = sub(oldI,R); initialize();
			print "--> Calculated multiplicity of a solution";
			return m;
		);
	);
	return 0;
	
);
