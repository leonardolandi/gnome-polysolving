str := get inputfile;
arr := value(str);
print concatenate("\n--> Received search coordinates of interval (",toString(arr#0),",",toString(arr#1),")");
c := poly(concatenate("-(x-",toString(arr#0),")(x-",toString(arr#1),")"));
outputfile << "";
outputfile << count << endl;
outputfile << 9 << endl;
outputfile << toString(arr) << endl;
sol := sylvester(c,2);
outputfile << sol << endl;
print "--> Calculated number of solutions inside interval";
vals := new List;
if sol > 0 and arr#2 != 0 then (
	vals = isolate(arr#0,arr#1,vals);
	print "--> Isolated solutions inside sub-intervals";
	bis := new List;
	for int in vals do bis = append(bis,bisect(int#0,int#1,arr#2));
	print concatenate("--> Calculated approximations with precision ",toString(arr#2));
	outputfile << toString(vals) << endl;
	outputfile << toString(bis) << endl;
);
if sol > 0 and arr#3 != 0 then (
	if #vals == 0 then (
		vals = isolate(arr#0,arr#1,vals);
		outputfile << toString(vals) << endl;
	);	
	mult := new List;	
	for val in vals do mult = append(mult,multip(val#0,val#1));
	print "--> Calculated multiplicities";
	outputfile << toString(mult) << endl;
);
outputfile << close;
count = count +1;
