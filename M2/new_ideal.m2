str := get inputfile;
print concatenate("\n--> Received ideal ",substring(str,1,#str)," in ",str#0," variables");
if str#0 == "1" then R = QQ[x] else R = QQ[x,y];
outputfile << "";
outputfile << count << endl;
try (
	I = sub(ideal(value substring(str,2,#str-3)),R);
	if dim(I) == 0 then (
		initialize();
		print "--> Calculated quotient basis";
		outputfile << 0 << endl;
		outputfile << toString(I) << endl;
		outputfile << D << endl;
		outputfile << sylvester(1,1) << endl;
		outputfile << sylvester(1,2) << endl;
		if str#0 == "2" then (
			outputfile << degree I << endl;
			outputfile << toString(gens gb I) << endl;
			outputfile << toString(gens gb rad()) << endl;
			outputfile << toString(gens gb Ix) << endl;
			outputfile << toString(gens gb Iy) << endl;
		);
		print "--> Calculated Sylvester-Hermite infos";
	) else if dim(I) == -1 then(
		outputfile << 2 << endl;
		print "--> Given ideal has no solutions";
	) else (
		outputfile << 1 << endl;
		print "--> Given ideal is not zero-dimensional";
	);
) else (
	outputfile << 3 << endl;
	if str#0 == "1" then print "--> Polynomial is not written correctly" else print "--> Ideal generators are not written correctly";
);
outputfile << close;
count = count +1;
