str := get inputfile;
print concatenate("\n--> Received polynomial h = ",str);
outputfile << "";
outputfile << count << endl;
try (
	h = sub(value(str),R);
	outputfile << 4 << endl;
	outputfile << h << endl;
	outputfile << sylvester(h,1) << endl;
	outputfile << sylvester(h,2) << endl;
	outputfile << sylvester(h,3) << endl;
	print concatenate("--> Calculated Sylvester-Hermite with polynomial h = ",str);
) else (
	outputfile << 5 << endl;
	print "--> Polynomial h is not written correctly";
);
outputfile << close;
count = count +1;
