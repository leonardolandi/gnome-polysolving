prefix = /usr

APPDIR = $(prefix)/lib/PolySolving
BINDIR = $(prefix)/bin
ICONDIR = $(prefix)/share/icons/hicolor
DESKTOPDIR = $(prefix)/share/applications
LICENSEDIR = $(prefix)/share/licenses/PolySolving
LANGDIR = $(prefix)/share/locale

check:

	@# check if packages are installed
	@for i in python3 msgfmt; do \
		if [ ! $$(which $$i 2> /dev/null) ]; then \
			echo ""; \
			echo \'$$i\' is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			false; \
		fi; \
	done;

	@# check for python modules
	@for i in sys os gi gi.repository locale subprocess time threading cairo math fractions; do \
		python3 -c "import "$$i 2> /dev/null; \
		if [ $$? = 1 ]; then \
			echo ""; \
			echo python \'$$i\' module is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			false; \
		fi; \
	done;

	@# check if Macaulay2 is installed
	@if [ ! -n "$$(type -p M2)" ]; then \
		echo ""; \
		echo Macaulay2 is not installed on your system.; \
		echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
		echo You can also use the \'Generic\' version of Macaulay2, which does not require installation.; \
		echo You will be able to choose the \'Generic\' version of Macaulay2 during the program startup.; \
		echo ""; \
	fi;

install:

	@# check if packages are installed
	@echo -ne Checking for dependencies...;
	@for i in python3 msgfmt; do \
		if [ ! $$(which $$i 2> /dev/null) ]; then \
			echo -e "\n"; \
			echo ERROR: \'$$i\' is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			exit 1; \
		fi; \
	done;
	@echo -ne " "ok'\n';

	@# check for python modules
	@echo -ne Checking for python modules...;
	@for i in sys os gi gi.repository locale subprocess time threading cairo math fractions; do \
		python3 -c "import "$$i 2> /dev/null; \
		if [ $$? = 1 ]; then \
			echo -e "\n"; \
			echo ERROR: python \'$$i\' module is not installed on your system.; \
			echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
			echo ""; \
			exit 1; \
		fi; \
	done;
	@echo -ne " "ok'\n';

	@# copy python files
	@echo -ne Installing libraries...;
	@mkdir -p $(APPDIR)/python;
	@cp "$(CURDIR)"/lib/* $(APPDIR)/python/;
	@sed -i "s|LANGUAGE_FOLDER|\"$(LANGDIR)\"|g" $(APPDIR)/python/translation.py

	@# copy M2 files
	@mkdir -p $(APPDIR)/M2;
	@cp "$(CURDIR)"/M2/* $(APPDIR)/M2/;

	@# copy glade files
	@mkdir -p $(APPDIR)/glade;
	@cp "$(CURDIR)"/ui/* $(APPDIR)/glade/;
	@echo -ne " "ok'\n';

	@# copy icons
	@echo -ne Installing icons...;
	@for i in 16x16 32x32 48x48 64x64 128x128 256x256 scalable symbolic; do \
		mkdir -p $(ICONDIR)/$$i/apps; \
		cp "$(CURDIR)"/icons/$$i/* $(ICONDIR)/$$i/apps/; \
	done;
	@echo -ne " "ok'\n';

	@# copy languages
	@echo -ne Installing languages...;
	@for i in "$(CURDIR)"/po/*.po; do \
		h=$${i%.po}; \
		j=$${h##*/}; \
		mkdir -p $(LANGDIR)/$$j/LC_MESSAGES; \
		msgfmt "$$i" -o $(LANGDIR)/$$j/LC_MESSAGES/polysolving.mo; \
	done;
	@echo -ne " "ok'\n';

	@# create executable file
	@echo -ne Installing executable...;
	@mkdir -p $(BINDIR);
	@cp "$(CURDIR)"/bin/polysolving $(BINDIR)/;
	@sed -i "s|appdir|$(APPDIR)|g" $(BINDIR)/polysolving;
	@chmod +x $(BINDIR)/polysolving;
	@echo -ne " "ok'\n';

	@# copy desktop launcher
	@echo -ne Installing desktop launcher...;
	@mkdir -p $(DESKTOPDIR);
	@desktop-file-install "$(CURDIR)"/PolySolving.desktop;
	@update-desktop-database;
	@echo -ne " "ok'\n';

	@# copy license
	@echo -ne Installing license...;
	@mkdir -p $(LICENSEDIR);
	@cp "$(CURDIR)"/LICENSE $(LICENSEDIR)/;
	@echo -ne " "ok'\n';

	@# update icon cache
	@echo -ne Updating icon cache...;
	@gtk-update-icon-cache -q -t -f $(ICONDIR);
	@echo -ne " "ok'\n';

	@# create folder for archives in users home
	@echo -ne Creating users folders...;
	@for i in /home/*/; do \
		if [ -w "$$i" ]; then \
			mkdir -p $$i.PolySolving; \
			chmod 777 -R $$i.PolySolving; \
		fi; \
	done;
	@echo -ne " "ok'\n';

	@# check if Macaulay2 is installed
	@echo -ne Checking for Macaulay2...;
	@if [ ! -n "$$(type -p M2)" ]; then \
		echo -e "\n"; \
		echo WARNING: Macaulay2 is not installed on your system.; \
		echo Please install the corresponding package for your distribution and then run \'make install\' again.; \
		echo You can also use the \'Generic\' version of Macaulay2, which does not require installation.; \
		echo You will be able to choose the \'Generic\' version of Macaulay2 during the program startup.; \
		echo ""; \
	else \
		echo -ne " "ok'\n'; \
	fi;

	@# display an ending message
	@echo PolySolving was successfully installed.

uninstall:

	@rm -rf $(APPDIR);
	@rm -rf $(BINDIR)/polysolving;
	@rm -f $(DESKTOPDIR)/PolySolving.desktop;
	@for i in 16x16 32x32 48x48 64x64 128x128 256x256; do rm -f $(ICONDIR)/$$i/apps/polysolving.png; done;
	@rm -f $(ICONDIR)/scalable/apps/polysolving.svg;
	@rm -f $(ICONDIR)/symbolic/apps/polysolving-symbolic.svg;
	@rm -rf $(LICENSEDIR);
	@for i in "$(CURDIR)"/po/*.po; do \
		h=$${i%.po}; \
		j=$${h##*/}; \
		rm -f $(LANGDIR)/$$j/LC_MESSAGES/polysolving.mo; \
	done;
	@echo PolySolving was successfully uninstalled.;
