# functions.py
# This file is part of GNOME-PolySolving
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def parse_polynomial(p):
	p = p.replace(" ","")
	l = len(p)
	if l is 0: return None # Zero-length
	if p[0] in "*/^.)": return None # Prohibited first characters
	if p[l-1] in "+-*/^(.": return None # Prohibited last characters
	for i in range(l):
		if p[i] not in "xy+-*/^.()0123456789": return None # Characters allowed
		if i < l-1:
			if p[i] in "xy" and p[i+1] is ".": return None # No dot after variable
			if p[i] in "+-*/^" and p[i+1] in "+-*/^.)": return None # Prohibited symbols after operator
			if p[i] is "/" and p[i+1] in "xy": return None # No variable after division
			if p[i] is "^" and p[i+1] in "xy": return None # No variable after power
			if p[i] is "." and p[i+1] in "xy+-*/^.()": return None # Only number after dot
			if p[i] is "(" and p[i+1] in "*/^.)": return None # Prohibited symbols after open par
			if p[i] is ")" and p[i+1] is ".": return None # No dot after closed par
			if p[i] in "0123456789" and p[i+1] is "^": return None # No power symbol after number
	# Parenthesis control
	par = 0
	for i in range(l):
		if p[i] is "(": par += 1
		elif p[i] is ")": par -= 1
		if par < 0: return None
	if par is not 0: return None
	# Dot control
	i = 0
	while i < l-1:
		if p[i] is ".":
			j = i+1
			while j < l-1 and p[j] in "0123456789": j += 1
			if j < l-1 and p[j] is ".": return None
			else: i = j
		else: i += 1
	# Check if p has only numbers after division or power
	i = 0
	while i < l-1:
		if p[i] is "/" or p[i] is "^":
			if p[i+1] in "0123456789":
				j = i+2
				while j < l and p[j] in "0123456789": j += 1
				if j < l and p[j] in "xy": return None
			else:
				par = 1
				j = i+2
				while j < l:
					if p[j] is "(": par += 1
					elif p[j] is ")": par -= 1
					elif p[j] in "xy" and par > 0: return None
					if par is 0: break
					j += 1
		i += 1
	# Check if exponent is a positive integer
	i = 0
	while i < l-1:
		if p[i] is "^":
			if p[i+1] is "(":
				par = 1
				exp = ""
				j = i+2
				while j < l and par > 0:
					if p[j] is "(": par += 1
					elif p[j] is ")": par -= 1
					exp += p[j]
					j += 1
				try:
					if int(exp[:-1]) < 0: return None
				except ValueError: return None
			else:
				j = i+1
				while j < l and p[j] in "0123456789": j += 1
				if j < l and p[j] is ".": return None
		i += 1
	# Insert * symbol when missing
	i = 0
	while i < l-1:
		if (p[i] in "xy" and p[i+1] in "xy(0123456789") or (p[i] in "0123456789" and p[i+1] in "xy(") or (p[i] is ")" and p[i+1] in "xy(0123456789"):
			p = p[:(i+1)] + "*" + p[(i+1):]
			l = len(p)
			i = 0
		else: i += 1
	# Convert coefficients from decimal to fraction
	i = 0
	while i < l-1:
		if p[i] is ".":
			num = ""
			den = "1"
			j = i-1
			while j > -1 and p[j] in "0123456789":
				num = p[j] + num
				j -= 1
			decimal = num + "."
			j = i+1
			while j < l and p[j] in "0123456789":
				num += p[j]
				den += "0"
				decimal += p[j]
				j += 1
			p = p.replace(decimal,"(" + num + "/" + den + ")")
			l = len(p)
			i = 0
		else: i += 1
	return p

def parse_ideal(string):
	d = string[9:][:-2].replace(" ","").split(",")
	s = "("
	for pol in d: s += pol + ","
	s = s[:-1] + ")"
	return s
