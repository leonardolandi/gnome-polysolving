# -*- coding: utf-8 -*-

# main_window.py
# This file is part of GNOME-PolySolving
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi, os, sys, subprocess, translation
from time import time, sleep
from math import floor
from fractions import Fraction
from threading import Thread
from locale import gettext as _
gi.require_version("Gtk","3.0")
gi.require_version("Vte","2.91")
from gi.repository import Gtk, Gdk, Vte, GLib
from tab import Tab
from row import Row
from about import About
from functions import parse_polynomial, parse_ideal

class MainWindow(Gtk.ApplicationWindow):

	def __init__(self,application):

		self.APP_FOLDER = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		CONF_FOLDER = os.getenv("HOME") + "/.PolySolving"
		Gtk.Window.__init__(self, application=application)
		builder = Gtk.Builder()
		builder.add_from_file(self.APP_FOLDER + "/glade/main_window.glade")
		builder.connect_signals(self)
		builder.set_translation_domain("polysolving")

#		M2 CONFIGURATION FILES
		M2 = ""
		with open(CONF_FOLDER + "/M2location","r") as f: M2 = f.read()[:-1]
		f.close()
		self.inputfile = CONF_FOLDER + "/input_file"
		self.outputfile = CONF_FOLDER + "/output_file"

#		WIDGETS
		self.main_window = builder.get_object("main_window")
		self.top_popover = builder.get_object("top_popover")

		self.upper_label = builder.get_object("upper_label")
		self.modify_box = builder.get_object("modify_box")
		self.ideal_entry = builder.get_object("ideal_entry")
		self.ideal_label = builder.get_object("ideal_label")
		self.ideal_button = builder.get_object("ideal_button")

		self.notebook = builder.get_object("notebook")
		self.listbox = builder.get_object("listbox")

		self.poly_h_radio = builder.get_object("poly_h_radio")
		self.poly_h_revealer = builder.get_object("poly_h_revealer")
		self.poly_h_entry = builder.get_object("poly_h_entry")
		self.poly_h_button = builder.get_object("poly_h_button")

		self.rect_radio = builder.get_object("rect_radio")
		self.rect_revealer = builder.get_object("rect_revealer")
		self.ax_entry = builder.get_object("ax_entry")
		self.bx_entry = builder.get_object("bx_entry")
		self.ay_entry = builder.get_object("ay_entry")
		self.by_entry = builder.get_object("by_entry")
		self.rect_button = builder.get_object("rect_button")

		self.circ_radio = builder.get_object("circ_radio")
		self.circ_revealer = builder.get_object("circ_revealer")
		self.x_entry = builder.get_object("x_entry")
		self.y_entry = builder.get_object("y_entry")
		self.r_entry = builder.get_object("r_entry")
		self.circ_button = builder.get_object("circ_button")

		self.int_radio = builder.get_object("int_radio")
		self.int_revealer = builder.get_object("int_revealer")
		self.left_entry = builder.get_object("left_entry")
		self.right_entry = builder.get_object("right_entry")
		self.int_button = builder.get_object("int_button")

		self.check_multip = builder.get_object("check_multip")
		self.check_approx = builder.get_object("check_approx")
		self.app_revealer = builder.get_object("app_revealer")
		self.app_scale = builder.get_object("app_scale")
		self.prec_label = builder.get_object("prec_label")

		self.terminal_widget = builder.get_object("terminal_widget")
		self.status_bar = builder.get_object("status_bar")
		self.spinner = builder.get_object("spinner")
		self.time_label = builder.get_object("time_label")

#		VTE TERMINAL
		self.terminal = Vte.Terminal()
		self.terminal.spawn_sync(Vte.PtyFlags.DEFAULT,os.environ["HOME"],[M2,"--silent"],[],GLib.SpawnFlags.DO_NOT_REAP_CHILD,None,None)
		self.terminal_widget.add(self.terminal)

#		GLOBAL VARIABLES
		self.numgens = 2
		self.editing_ideal = True
		self.waiting_result = True
		self.read_count = -1
		self.operation = ""
		self.initial_time = time()
		self.precision = "1/10"
		self.tabs = 1

#		PREPARE
		with open(self.outputfile,"w") as f: f.write("-1")
		f.close()
		self.main_window.show_all()
		self.notebook.hide()
		self.ideal_label.hide()
		self.int_radio.hide()
		self.int_revealer.hide()
		self.spinner.hide()
		self.app_scale.override_background_color(Gtk.StateFlags.NORMAL,Gdk.RGBA(0.33,0.66,0,1))

#		MAIN
		msg = 'load "' + CONF_FOLDER + '/conf.m2";\nload "' + self.APP_FOLDER + '/M2/functions.m2"\n'
		self.terminal.feed_child_binary(msg.encode())
		Gtk.main()

#	COMMUNICATION

	def send_to_m2(self,string,filename):
		self.buttons_enabled(False)
		self.ideal_button.set_sensitive(False)
		self.initial_time = time()
		with open(self.inputfile,"w") as f: f.write(string)
		f.close()
		msg = 'load "' + self.APP_FOLDER + '/M2/' + filename + '.m2";\n'
		self.terminal.feed_child_binary(msg.encode())
		self.waiting_result = True
		self.time_label.set_text("")
		Thread(target=self.read_from_file,).start()

	def read_from_file(self):
		GLib.idle_add(self.spinner.start,)
		GLib.idle_add(self.spinner.show,)
		while self.waiting_result:
			f = open(self.outputfile,"r")
			data = f.readlines()
			f.close()
			if len(data) and int(data[0]) is not self.read_count:
				self.editing_ideal = False
				self.read_count = int(data[0])
				now = time() - self.initial_time
				sec = '%.1f' % now
				GLib.idle_add(self.notify,self.operation, sec, False)
				for i in range(len(data)): data[i] = data[i][:-1]
# 				CALCULATED IDEAL
				if int(data[1]) is 0:
					items = []
					if self.numgens is 1:
						poly = data[2][6:][:-1] if "(" in data[2] else data[2][6:]
						items.append([_("Polynomial"),poly])
						GLib.idle_add(self.ideal_label.set_text,poly)
					else:
						items.append([_("System ideal"),data[2][5:]])
						GLib.idle_add(self.ideal_label.set_text,data[2][6:][:-1])
					if self.numgens is 2:
						items.append([_("Degree of ideal"),data[6]])
						items.append([_("Gröbner basis"),parse_ideal(data[7])])
						items.append([_("Radical of ideal"),parse_ideal(data[8])])
						items.append([_("First elimination ideal"),parse_ideal(data[9])])
						items.append([_("Second elimination ideal"),parse_ideal(data[10])])
					diff = int(data[4]) - int(data[5])
					items.append([_("Number of solutions"),data[3]])
					items.append([_("Number of distinct solutions"),data[4]])
					items.append([_("Number of real distinct solutions"),data[5]])
					items.append([_("Number of non-real distinct solutions"),str(diff)])
					self.notebook.show()
					GLib.idle_add(self.fill_result,items)
					GLib.idle_add(self.modify_box.hide,)
					GLib.idle_add(self.ideal_label.show,)
					GLib.idle_add(self.ideal_button.set_label,_("Modify"))
#				NON ZERO-DIMENSIONAL IDEAL
				elif int(data[1]) is 1:
					GLib.idle_add(self.notify,_("Inserted ideal is not zero-dimensional. Insert a new one."), sec, True)
					self.editing_ideal = True
#				IDEAL WITHOUT SOLUTIONS
				elif int(data[1]) is 2:
					GLib.idle_add(self.notify,_("Inserted ideal has no solutions. Insert a new one."), sec, True)
					self.editing_ideal = True
#				IDEAL WRITTEN BAD
				elif int(data[1]) is 3:
					s = ""
					if self.numgens is 2: s = _("Ideal generators are not written correctly.")
					else: s = _("Polynomial is not written correctly.")
					GLib.idle_add(self.notify,s, "", True)
					GLib.idle_add(self.ideal_entry.get_style_context().add_class,"error")
					GLib.idle_add(self.ideal_entry.grab_focus,)
					self.editing_ideal = True
#				POLYNOMIAL H CALCULATED
				elif int(data[1]) is 4:
					string = _("Polynomial") + " h: " + data[2].replace(" ","") + "\n"
					string += _("Number of solutions (real or non-real) where")  + " h ≠ 0: " + data[3] + "\n"
					string += _("Number of real and distinct solutions where") + " h > 0: " + data[4] + "\n"
					string += _("Number of real and distinct solutions where") + " h < 0: " + data[5]
					GLib.idle_add(self.new_tab,data,string)
#				POLYNOMIAL H WRITTEN BAD
				elif int(data[1]) is 5:
					GLib.idle_add(self.notify,_("Polynomial h is not written correctly."), "", True)
					GLib.idle_add(self.poly_h_entry.get_style_context().add_class,"error")
					GLib.idle_add(self.poly_h_entry.grab_focus,)
#				SOLUTIONS IN RECTANGLE
				elif int(data[1]) is 6:
					if int(data[3]) is 0: GLib.idle_add(self.new_tab,data,_("There are no solutions inside the given rectangle."))
					elif int(data[3]) is 1: GLib.idle_add(self.new_tab,data,_("There is only one solution inside the given rectangle."))
					else: GLib.idle_add(self.new_tab,data,_("There are") + " " + data[3] + " " + _("distinct solutions inside the given rectangle."))
#				SOLUTION ON BORDER OF RECTANGLE
				elif int(data[1]) is 7:
					GLib.idle_add(self.notify,_("The endpoint") + " " + data[4] + "=" + data[3] + " " + _("belongs to a solution. Choose an other one."), sec, True)
#				SOLUTIONS INSIDE CIRCLE
				elif int(data[1]) is 8:
					if int(data[3]) is 0: GLib.idle_add(self.new_tab,data,_("There are no solutions inside the given circle."))
					elif int(data[3]) is 1: GLib.idle_add(self.new_tab,data,_("There is only one solution inside the given circle."))
					else: GLib.idle_add(self.new_tab,data,_("There are") + " " + data[3] + " " + _("distinct solutions inside the given circle."))
#				SOLUTION INDISE INTERVAL
				elif int(data[1]) is 9:
					if int(data[3]) is 0: GLib.idle_add(self.new_tab,data,_("There are no solutions in the given interval."))
					elif int(data[3]) is 1: GLib.idle_add(self.new_tab,data,_("There is only one solution in the given interval."))
					else: GLib.idle_add(self.new_tab,data,_("There are") + " " + data[3] + " " + _("distinct solutions in the given interval."))
				self.waiting_result = False
		GLib.idle_add(self.ideal_button.set_sensitive,True)
		GLib.idle_add(self.restore_sensitivity,)
		GLib.idle_add(self.spinner.stop,)
		GLib.idle_add(self.spinner.hide,)

#	UTILS

	def new_tab(self,data,string):
		tab = Tab(data,string,self.tabs)
		ind = self.notebook.append_page(tab.main_box,tab.box_label)
		self.notebook.set_current_page(ind)
		self.tabs += 1

	def notify(self,text,time,error):
		if error: self.status_bar.set_markup("<b>" + text + "</b>")
		else: self.status_bar.set_text(text)
		if len(time) is 0: self.time_label.set_text("")
		else: self.time_label.set_text(time + " " + _("seconds"))

	def buttons_enabled(self,enabled):
		self.poly_h_button.set_sensitive(enabled)
		self.rect_button.set_sensitive(enabled)
		self.circ_button.set_sensitive(enabled)
		self.int_button.set_sensitive(enabled)

	def fill_result(self,items):
		for row in self.listbox.get_children(): self.listbox.remove(row)
		for item in items: self.listbox.insert(Row(item[0],item[1]).row,-1)

	def restore_sensitivity(self):
		self.on_poly_h_entry_changed(None)
		self.on_interval_entry_changed(None)
		self.on_circle_entry_changed(None)
		self.on_rectangle_entry_changed(None)

#	TOP WINDOW SIGNALS

	def on_main_window_destroy(self,widget):
		Gtk.main_quit()
		sys.exit(0)

	def on_top_button_toggled(self,widget): self.top_popover.set_visible(widget.get_active())

	def on_top_popover_closed(self,widget): widget.get_relative_to().set_active(False)

	def on_menu_about_clicked(self,widget):
		dialog = About(self)
		if dialog.window.run(): dialog.window.destroy()

	def on_ideal_entry_changed(self,widget): self.ideal_entry.get_style_context().remove_class("error")

	def on_ideal_button_clicked(self,widget):
		if self.editing_ideal:
			if self.numgens is 2:
				gens = self.ideal_entry.get_text().split(",")
				for i in range(len(gens)): gens[i] = parse_polynomial(gens[i])
				if None in gens:
					self.notify(_("Ideal generators are not written correctly."), "", True)
					self.ideal_entry.get_style_context().add_class("error")
					self.ideal_entry.grab_focus()
				else:
					ideal = "("
					for g in gens: ideal += g + ","
					ideal = ideal[:-1] + ")"
					self.operation = _("Calculated ideal") + " " + ideal + " " +_("and general informations about solutions.")
					self.notify(_("Calculating ideal") + " " + ideal + " " + _("and informations about solutions") + ". " + _("Please wait") + "...", "", False)
					self.send_to_m2(str(self.numgens) + ideal,"new_ideal")
			else:
				poly = parse_polynomial(self.ideal_entry.get_text())
				if poly is None or "y" in poly:
					self.notify(_("Polynomial is not written correctly."), "", True)
					self.ideal_entry.get_style_context().add_class("error")
					self.ideal_entry.grab_focus()
				else:
					self.operation = _("Calculated polynomial") + " " + poly + " " + _("and general informations about solutions.")
					self.notify(_("Calculating polynomial") + " " + poly + " " + _("and informations about solutions") + ". " + _("Please wait") + "...", "", False)
					self.send_to_m2(str(self.numgens) + "(" + poly + ")","new_ideal")
		else:
			self.ideal_entry.set_text(self.ideal_label.get_text())
			self.ideal_label.hide()
			self.modify_box.show()
			self.ideal_button.set_label(_("Submit"))
			self.editing_ideal = True
			self.buttons_enabled(False)
			self.ideal_button.set_sensitive(True)

	def on_numgens_changed(self,widget):
		self.numgens = int(widget.get_active_id())
		if self.numgens is 1:
			self.upper_label.set_text(_("Polynomial") + ":")
			self.ideal_entry.set_placeholder_text(_("Enter polynomial"))
			self.ideal_entry.set_tooltip_text(_("Enter here polynomial to be computed"))
			self.rect_radio.hide()
			self.rect_revealer.hide()
			self.circ_radio.hide()
			self.circ_revealer.hide()
			self.int_radio.show()
			self.int_revealer.show()
			self.on_poly_h_radio_toggled(None)
			self.poly_h_radio.set_active(True)
		else:
			self.upper_label.set_text(_("Ideal generators") + ":")
			self.ideal_entry.set_placeholder_text(_("Enter ideal generators"))
			self.ideal_entry.set_tooltip_text(_("Enter here ideal generators, separated by commas. Generators f(x,y),g(x,y),... are polynomials corresponding to the equations of the system f(x,y)=g(x,y)=...=0"))
			self.int_radio.hide()
			self.int_revealer.hide()
			self.rect_radio.show()
			self.rect_revealer.show()
			self.circ_radio.show()
			self.circ_revealer.show()
			self.on_poly_h_radio_toggled(None)
			self.poly_h_radio.set_active(True)

#	LOCALIZE RADIO BUTTONS

	def on_poly_h_radio_toggled(self,widget):
		self.check_multip.set_sensitive(False)
		self.check_approx.set_sensitive(False)
		self.rect_revealer.set_reveal_child(False)
		self.circ_revealer.set_reveal_child(False)
		self.int_revealer.set_reveal_child(False)
		self.app_revealer.set_reveal_child(False)
		self.check_multip.set_active(False)
		self.check_approx.set_active(False)
		self.poly_h_revealer.set_reveal_child(True)

	def on_rect_radio_toggled(self,widget):
		self.check_multip.set_sensitive(True)
		self.check_approx.set_sensitive(True)
		self.poly_h_revealer.set_reveal_child(False)
		self.circ_revealer.set_reveal_child(False)
		self.rect_revealer.set_reveal_child(True)

	def on_circ_radio_toggled(self,widget):
		self.check_multip.set_sensitive(False)
		self.check_approx.set_sensitive(False)
		self.poly_h_revealer.set_reveal_child(False)
		self.rect_revealer.set_reveal_child(False)
		self.app_revealer.set_reveal_child(False)
		self.check_multip.set_active(False)
		self.check_approx.set_active(False)
		self.circ_revealer.set_reveal_child(True)

	def on_int_radio_toggled(self,widget):
		self.check_multip.set_sensitive(True)
		self.check_approx.set_sensitive(True)
		self.poly_h_revealer.set_reveal_child(False)
		self.int_revealer.set_reveal_child(True)

	def on_check_approx_toggled(self,widget):
		self.app_revealer.set_reveal_child(widget.get_active())

	def on_app_adjustment_value_changed(self,widget):
		val = widget.get_value()
		val = float('%.2f' % round(val,2))
		self.app_scale.override_background_color(Gtk.StateFlags.NORMAL,Gdk.RGBA(-val/3,1+val/3,0,1))
		prec = pow(10,val)
		prec = round(prec,-int(floor(val)))
		p = "1" if prec == 1 else str(prec)
		self.prec_label.set_text("±" + p)
		self.precision = str(int(prec*1000)) + "/1000"

#	LOCALIZE ENTRIES

	def on_poly_h_entry_changed(self,widget):
		if widget is not None: widget.get_style_context().remove_class("error")
		if not self.waiting_result and not self.editing_ideal:
			self.poly_h_button.set_sensitive(len(self.poly_h_entry.get_text()) > 0)

	def on_interval_entry_changed(self,widget):
		if not self.waiting_result and not self.editing_ideal:
			try:
				l = float(self.left_entry.get_text())
				r = float(self.right_entry.get_text())
				self.int_button.set_sensitive(l<r)
			except ValueError: self.int_button.set_sensitive(False)

	def on_circle_entry_changed(self,widget):
		if not self.waiting_result and not self.editing_ideal:
			try:
				x = float(self.x_entry.get_text())
				y = float(self.y_entry.get_text())
				r = float(self.r_entry.get_text())
				self.circ_button.set_sensitive(r>0)
			except ValueError: self.circ_button.set_sensitive(False)

	def on_rectangle_entry_changed(self,widget):
		if not self.waiting_result and not self.editing_ideal:
			try:
				ax = float(self.ax_entry.get_text())
				bx = float(self.bx_entry.get_text())
				ay = float(self.ay_entry.get_text())
				by = float(self.by_entry.get_text())
				self.rect_button.set_sensitive(ax<bx and ay<by)
			except ValueError: self.rect_button.set_sensitive(False)

# LOCALIZE CONFIRM BUTTONS

	def on_poly_h_button_clicked(self,widget):
		poly = parse_polynomial(self.poly_h_entry.get_text())
		if poly is None or (self.numgens is 1 and "y" in poly):
			self.notify(_("Polynomial h is not written correctly."), "", True)
			self.poly_h_entry.get_style_context().add_class("error")
			self.poly_h_entry.grab_focus()
		else:
			self.operation = _("Localized solutions where") + " " + poly + " > 0."
			self.notify(_("Localizing solutions where") + " " + poly + " > 0. " + _("Please wait") + "...", "", False)
			self.send_to_m2(poly,"h_poly")

	def on_rect_button_clicked(self,widget):
		ax = str(Fraction(self.ax_entry.get_text()))
		bx = str(Fraction(self.bx_entry.get_text()))
		ay = str(Fraction(self.ay_entry.get_text()))
		by = str(Fraction(self.by_entry.get_text()))
		p = str(self.precision) if self.check_approx.get_active() else "0"
		m = "1" if self.check_multip.get_active() else "0"
		string = ax + "," + bx + "," + ay + "," + by + "," + p + "," + m
		self.operation = _("Found number of solutions inside rectangle.")
		self.notify(_("Search of solutions inside rectangle in progress") + ". " + _("Please wait") + "...", "", False)
		self.send_to_m2(string,"rectangle")

	def on_circ_button_clicked(self,widget):
		x = str(Fraction(self.x_entry.get_text()))
		y = str(Fraction(self.y_entry.get_text()))
		r = str(Fraction(self.r_entry.get_text()))
		string = x + "," + y + "," + r
		self.operation = _("Found number of solutions inside circle.")
		self.notify(_("Search of solutions inside circle in progress") + ". " + _("Please wait") + "...", "", False)
		self.send_to_m2(string,"circle")

	def on_int_button_clicked(self,widget):
		l = str(Fraction(self.left_entry.get_text()))
		r = str(Fraction(self.right_entry.get_text()))
		p = str(self.precision) if self.check_approx.get_active() else "0"
		m = "1" if self.check_multip.get_active() else "0"
		string = l + "," + r + "," + p + "," + m
		self.operation = _("Found number of solutions inside interval.")
		self.notify(_("Search of solutions inside interval in progress") + ". " + _("Please wait") + "...", "", False)
		self.send_to_m2(string,"interval")
