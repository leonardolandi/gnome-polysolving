# tab.py
# This file is part of GNOME-PolySolving
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cairo, gi
from os import path
from math import pi
from fractions import Fraction
from locale import gettext as _
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

class Tab():

	def __init__(self,data,string,num):

		# TAB
		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/tab.glade")
		builder.connect_signals(self)

		self.box_label = builder.get_object("box_label")
		self.tab_label = builder.get_object("tab_label")

		self.main_box = builder.get_object("main_box")

		self.label = builder.get_object("label")

		self.table_scrolledwindow = builder.get_object("table_scrolledwindow")
		self.rectangle_column = builder.get_object("rectangle_column")
		self.approx_column = builder.get_object("approx_column")
		self.multip_column = builder.get_object("multip_column")
		self.liststore = builder.get_object("liststore")

		self.graph_box = builder.get_object("graph_box")
		self.canvas = builder.get_object("canvas")
		self.menu_button = builder.get_object("menu_button")
		self.menu_popover = builder.get_object("menu_popover")

		# VARIABLES
		self.db = None
		self.zoom = 10
		self.distance = 0
		self.origin = [0,0]
		self.aux_origin = [0,0]
		self.point = [0,0]
		self.numbers = True
		self.notch = 1
		self.bg = [1,1,1]
		self.ax = [0,0,0]
		self.fg = [0.8,0,0]

		# INIT
		self.tab_label.set_text(_("Result") + " " + str(num))
		self.label.set_text(string)
		self.content = self.fill_table(data)

#	TABLE

	def fill_table(self,data):
		if data[1] is "6": # RECTANGLE
			r = data[2][1:][:-1].split(",")
			a = r[4]
			m = r[5]
			r.pop()
			r.pop()
			if a is not "0" and m is "1" and data[3] is not "0":
				vals = data[4][2:][:-2].replace(" ","").split("},{")
				apps = data[5][2:][:-2].replace(" ","").split("},{")
				mult = data[6][1:][:-1].replace(" ","").split(",")
				for i in range(len(vals)):
					vals[i] = vals[i].split(",")
					apps[i] = apps[i].split(",")
					self.liststore.append(["(" + vals[i][0] + "," + vals[i][1] + ") x (" + vals[i][2] + "," + vals[i][3] + ")","1","(" + apps[i][0] + "," + apps[i][1] + ")",mult[i]])
				return ["r",vals,mult,r]
			if a is not "0" and data[3] is not "0":
				self.multip_column.set_visible(False)
				vals = data[4][2:][:-2].replace(" ","").split("},{")
				apps = data[5][2:][:-2].replace(" ","").split("},{")
				for i in range(len(vals)):
					vals[i] = vals[i].split(",")
					apps[i] = apps[i].split(",")
					self.liststore.append(["(" + vals[i][0] + "," + vals[i][1] + ") x (" + vals[i][2] + "," + vals[i][3] + ")","1","(" + apps[i][0] + "," + apps[i][1] + ")",""])
				return ["r",vals,None,r]
			if m is "1" and data[3] is not "0":
				self.approx_column.set_visible(False)
				vals = data[4][2:][:-2].replace(" ","").split("},{")
				mult = data[5][1:][:-1].replace(" ","").split(",")
				for i in range(len(vals)):
					vals[i] = vals[i].split(",")
					self.liststore.append(["(" + vals[i][0] + "," + vals[i][1] + ") x (" + vals[i][2] + "," + vals[i][3] + ")","1","",mult[i]])
				return ["r",vals,mult,r]
			self.approx_column.set_visible(False)
			self.multip_column.set_visible(False)
			self.liststore.append(["(" + r[0] + "," + r[1] + ") x (" + r[2] + "," + r[3] + ")",data[3],"",""])
			return ["r",None,data[3],r]
		if data[1] is "9": # INTERVAL
			self.rectangle_column.set_title(_("Interval"))
			r = data[2][1:][:-1].split(",")
			a = r[2]
			m = r[3]
			r.pop()
			r.pop()
			if a is not "0" and m is "1" and data[3] is not "0":
				vals = data[4][2:][:-2].replace(" ","").split("},{")
				apps = data[5][1:][:-1].replace(" ","").split(",")
				mult = data[6][1:][:-1].replace(" ","").split(",")
				for i in range(len(vals)):
					vals[i] = vals[i].split(",")
					self.liststore.append(["(" + vals[i][0] + "," + vals[i][1] + ")","1",apps[i],mult[i]])
				return ["i",vals,mult,r]
			if a is not "0" and data[3] is not "0":
				self.multip_column.set_visible(False)
				vals = data[4][2:][:-2].replace(" ","").split("},{")
				apps = data[5][1:][:-1].replace(" ","").split(",")
				for i in range(len(vals)):
					vals[i] = vals[i].split(",")
					self.liststore.append(["(" + vals[i][0] + "," + vals[i][1] + ")","1",apps[i],""])
				return ["i",vals,None,r]
			if m is "1" and data[3] is not "0":
				self.approx_column.set_visible(False)
				vals = data[4][2:][:-2].replace(" ","").split("},{")
				mult = data[5][1:][:-1].replace(" ","").split(",")
				for i in range(len(vals)):
					vals[i] = vals[i].split(",")
					self.liststore.append(["(" + vals[i][0] + "," + vals[i][1] + ")","1","",mult[i]])
				return ["i",vals,mult,r]
			self.approx_column.set_visible(False)
			self.multip_column.set_visible(False)
			self.liststore.append(["(" +r[0] + "," + r[1] + ")",data[3],"",""])
			return ["i",None,data[3],r]
		if data[1] is "8": # CIRCLE
			self.rectangle_column.set_title(_("Circle"))
			r = data[2][1:][:-1].split(",")
			self.approx_column.set_visible(False)
			self.multip_column.set_visible(False)
			self.liststore.append(["C = (" +r[0] + "," + r[1] + ")  r = " + r[2],data[3],"",""])
			return ["c",None,data[3],r]
		self.graph_box.hide()
		self.table_scrolledwindow.hide()
		return ["n",None,None,None]

#	SIGNALS

	def on_close_button_clicked(self,widget):
		self.main_box.get_parent().remove(self.main_box)

	def on_eventbox_motion_notify_event(self,widget,event):
		w = widget.get_allocated_width()
		h = widget.get_allocated_height()
		self.origin = [self.aux_origin[0] + (self.point[0]-event.x)/self.distance, self.aux_origin[1] - (self.point[1]-event.y)/self.distance]
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()

	def on_eventbox_button_press_event(self,widget,event):
		if event.button is 1:
			self.point = [event.x,event.y]
			self.aux_origin = self.origin
		elif event.button is 3:
			self.menu.show_all()
			self.menu.popup(None, None, None, None, 0, Gtk.get_current_event_time())

	def on_adjustment_value_changed(self,widget):
		self.zoom = widget.get_value()
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()

	def on_canvas_configure_event(self, widget, event):
		self.db = cairo.ImageSurface(cairo.FORMAT_ARGB32,widget.get_allocated_width(),widget.get_allocated_height())
		cc = cairo.Context(self.db)
		w = self.db.get_width()
		h = self.db.get_height()
		self.distance = w/self.zoom
		self.draw_background(cc,w,h)
		self.draw_axes(cc,w,h)
		self.draw_content(cc,w,h)
		self.db.flush()

	def on_canvas_draw(self, widget, cr):
		cr.set_source_surface(self.db,0,0)
		cr.paint()

	def on_menu_button_toggled(self,widget): self.menu_popover.set_visible(widget.get_active())

	def on_menu_popover_closed(self,widget): widget.get_relative_to().set_active(False)

#	DRAW

	def draw_background(self,cc,w,h):
		cc.set_source_rgb(self.bg[0],self.bg[1],self.bg[2])
		cc.rectangle(0,0,w,h)
		cc.fill()
		cc.stroke()

	def draw_axes(self,cc,w,h):
		cc.set_source_rgb(self.ax[0],self.ax[1],self.ax[2])
		cc.set_line_width(1)
		if abs(self.origin[1]*self.distance) < h/2:
			cc.move_to(0, h/2 + self.origin[1]*self.distance)
			cc.line_to(w, h/2 + self.origin[1]*self.distance)
			n = h/2 + self.origin[1]*self.distance
			i = self.notch
			while (i-self.origin[0])*self.distance < w/2:
				t = w/2+(i-self.origin[0])*self.distance
				cc.move_to(t,n-4)
				cc.line_to(t,n+4)
				if self.numbers:
					cc.move_to(t,n-8)
					cc.show_text(str(i))
				i += self.notch
			i = self.notch
			while (i+self.origin[0])*self.distance < w/2:
				t = w/2-(i+self.origin[0])*self.distance
				cc.move_to(t,n-4)
				cc.line_to(t,n+4)
				if self.numbers:
					cc.move_to(t,n-8)
					cc.show_text("-" + str(i))
				i += self.notch
		if abs(self.origin[0]*self.distance) < w/2:
			cc.move_to(w/2 - self.origin[0]*self.distance,0)
			cc.line_to(w/2 - self.origin[0]*self.distance,h)
			if self.content[0] is not "i":
				n = w/2 - self.origin[0]*self.distance
				i = self.notch
				while (i-self.origin[1])*self.distance < h/2:
					t = h/2-(i-self.origin[1])*self.distance
					cc.move_to(n-4,t)
					cc.line_to(n+4,t)
					if self.numbers:
						cc.move_to(n+8,t)
						cc.show_text(str(i))
					i += self.notch
				i = self.notch
				while (i+self.origin[1])*self.distance < h/2:
					t = h/2+(i+self.origin[1])*self.distance
					cc.move_to(n-4,t)
					cc.line_to(n+4,t)
					if self.numbers:
						cc.move_to(n+8,t)
						cc.show_text("-" + str(i))
					i += self.notch
		cc.stroke()

	def draw_content(self,cc,w,h):
		vals = self.content[1]
		mult = self.content[2]
		r = self.content[3]
		if self.content[0] is "r":
			x = w/2-(self.origin[0]-Fraction(r[0]))*self.distance
			y = h/2+(self.origin[1]-Fraction(r[3]))*self.distance
			base = (Fraction(r[1])-Fraction(r[0]))*self.distance
			height = (Fraction(r[3])-Fraction(r[2]))*self.distance
			cc.set_source_rgb(self.fg[0],self.fg[1],self.fg[2])
			cc.rectangle(x,y,base,height)
			cc.stroke()
			if vals:
				for i in range(len(vals)):
					m = 0.1
					if mult and len(mult[i]) is 1:
						m = float("0." + mult[i])
					elif mult:
						m = 0.9
					x = w/2-(self.origin[0]-Fraction(vals[i][0]))*self.distance
					y = h/2+(self.origin[1]-Fraction(vals[i][3]))*self.distance
					base = (Fraction(vals[i][1])-Fraction(vals[i][0]))*self.distance
					height = (Fraction(vals[i][3])-Fraction(vals[i][2]))*self.distance
					cc.set_source_rgba(self.fg[0],self.fg[1],self.fg[2],m)
					cc.rectangle(x,y,base,height)
					cc.fill()
					cc.set_source_rgb(self.fg[0],self.fg[1],self.fg[2])
					cc.rectangle(x,y,base,height)
					cc.stroke()
			else:
				m = float("0." + mult) if len(mult) is 1 else 0.9
				cc.set_source_rgba(self.fg[0],self.fg[1],self.fg[2],m)
				cc.rectangle(x,y,base,height)
				cc.fill()
		elif self.content[0] is "i":
			x = w/2-(self.origin[0]-Fraction(r[0]))*self.distance
			base = (Fraction(r[1])-Fraction(r[0]))*self.distance
			cc.set_source_rgb(self.fg[0],self.fg[1],self.fg[2])
			cc.rectangle(x,-2,base,h+4)
			cc.stroke()
			if vals:
				for i in range(len(vals)):
					m = 0.1
					if mult and len(mult[i]) is 1:
						m = float("0." + mult[i])
					elif mult:
						m = 0.9
					x = w/2-(self.origin[0]-Fraction(vals[i][0]))*self.distance
					base = (Fraction(vals[i][1])-Fraction(vals[i][0]))*self.distance
					cc.set_source_rgba(self.fg[0],self.fg[1],self.fg[2],m)
					cc.rectangle(x,-2,base,h+4)
					cc.fill()
					cc.set_source_rgb(self.fg[0],self.fg[1],self.fg[2])
					cc.rectangle(x,-2,base,h+4)
					cc.stroke()
			else:
				m = float("0." + mult) if len(mult) is 1 else 0.9
				cc.set_source_rgba(self.fg[0],self.fg[1],self.fg[2],m)
				cc.rectangle(x,-2,base,h+4)
				cc.fill()
		elif self.content[0] is "c":
			m = float("0." + mult) if len(mult) is 1 else 0.9
			x = w/2-(self.origin[0]-Fraction(r[0]))*self.distance
			y = h/2+(self.origin[1]-Fraction(r[1]))*self.distance
			rad = Fraction(r[2])*self.distance
			cc.set_source_rgba(self.fg[0],self.fg[1],self.fg[2],m)
			cc.arc(x,y,rad,0,2*pi)
			cc.fill()
			cc.set_source_rgb(self.fg[0],self.fg[1],self.fg[2])
			cc.arc(x,y,rad,0,2*pi)
			cc.stroke()

#	COLOR WINDOW

	def on_switch_notch_state_set(self,widget,state):
		self.numbers = state
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()

	def on_notch_spin_value_changed(self,widget):
		self.notch = widget.get_value_as_int()
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()

	def on_bg_color_set(self,widget):
		color = widget.get_color()
		self.bg = [color.red/65535,color.green/65535,color.blue/65535]
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()

	def on_axes_color_set(self,widget):
		color = widget.get_color()
		self.ax = [color.red/65535,color.green/65535,color.blue/65535]
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()

	def on_figure_color_set(self,widget):
		color = widget.get_color()
		self.fg = [color.red/65535,color.green/65535,color.blue/65535]
		self.on_canvas_configure_event(self.canvas,None)
		self.canvas.queue_draw()
