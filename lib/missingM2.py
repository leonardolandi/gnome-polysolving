# missingM2.py
# This file is part of GNOME-PolySolving
#
# Copyright (C) 2016 Leonardo Landi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, gi, translation
from os import path
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

class MissingM2:

	def __init__(self):

		builder = Gtk.Builder()
		builder.add_from_file(path.dirname(path.dirname(path.abspath(__file__))) + "/glade/missingM2.glade")
		builder.connect_signals(self)

		window = builder.get_object("window")
		self.revealer = builder.get_object("revealer")
		self.done_button = builder.get_object("done_button")
		self.done_box = builder.get_object("done_box")
		self.executable = ""
		window.show_all()
		Gtk.main()

	def on_main_window_destroy(self,widget):
		Gtk.main_quit()
		sys.exit(0)

	def on_chooser_button_selection_changed(self,widget):
		name = widget.get_filename()
		if name.split("/")[-1] == "M2":
			self.done_box.hide()
			self.done_button.show()
			self.executable = name
		else:
			self.done_button.hide()
			self.done_box.show()
		self.revealer.set_reveal_child(True)

	def on_done_button_clicked(self,widget):
		Gtk.main_quit()
		print(self.executable)
		sys.exit(0)

MissingM2()
